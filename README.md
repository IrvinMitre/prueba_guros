# Prueba_Guros

En este repo se encuentran la api para comprobar si hay mutacion o no en un arreglo de secuencias de adn


## Uso en local
Para usar de forma local esta Api se debe hacer
-npm i
-nom run local



-Para comprobar si una secuencia tiene mutacion es el siguiente endpoint de tipo POST

http://localhost:8001/api/mutation
Ejemplo de dna para ingresar
{
"dna":["ATGCGT","CTGTTC","TTATTT","ATTCGG","GTGTCA","TTACTG"]

}

-Para optener el conteo de mutaciones esta el endpoint de tipo GET

http://localhost:8001/api/mutation


## Uso 


-Verificar una mutacion 
https://www.herokucdn.com/api/mutation
{
"dna":["ATGCGT","CTGTTC","TTATTT","ATTCGG","GTGTCA","TTACTG"]

}

-Optener la cantidad de mutaciones
https://pruebaguros.herokuapp.com/api/mutation

