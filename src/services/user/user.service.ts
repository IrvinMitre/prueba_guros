import userModel from "../../models/user.model";
import { UserInterface } from "../../models/user.interface";

class UserService {
  async login(email: String, password: String) {
    return await userModel.find({ email: email, password: password });
  }

  async register(user: UserInterface) {
    const user_new = new userModel(user);
    return await user_new.save();
  }
}

const userService = new UserService();
export default userService;
