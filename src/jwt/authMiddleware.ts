import { Response, Request, NextFunction } from "express";
import { verifyJWTToken } from "./auth";

const NOT_AUTH_401_MSG = "Failed to authenticate token.";
const NO_JWT_403_MSG = "No token provided.";

const authMiddleware = async (
  // there is any becouse we don't care about shape of user data
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { authorization = "" } = req.headers;
  const jwt = authorization.split(" ")[1];
  if (jwt) {
    try {
      const decoded = await verifyJWTToken(jwt);
      next();
    } catch (error) {
      res.status(401).json({
        success: false,
        message: NOT_AUTH_401_MSG,
      });
    }
  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: NO_JWT_403_MSG,
    });
  }
};

export default authMiddleware;
