import express from 'express'
import logger from 'morgan'
import bodyParser from 'body-parser'
import cors from 'cors'
import MutationRouter from './routes/mutation.routes'
import UserRouter from './routes/user.routes'
class App {
    public express: express.Application


    constructor() {
        this.express = express()
        this.middleware()
        this.routes()
     
    }

    private middleware() {
        this.express.use(cors())
        this.express.use(logger('dev'))
        this.express.use(bodyParser.json())
        this.express.use(bodyParser.urlencoded({
            extended: true
          }));
    }



    private routes() {
        const router: express.Router = express.Router()
        // placeholder route handler
        router.get('/', (req: express.Request, res: express.Response) => {
            res.json({
                message: 'Up!'
            })
        })
        this.express.use('/', router)
        this.express.use('/api/mutation', MutationRouter)
        this.express.use('/api/user', UserRouter)
       
    }

}

export default new App().express