import { UserInterface } from "./../../models/user.interface";
import { Request, Response } from "express";
import userService from "../../services/user/user.service";
import { createJWTToken } from "../../jwt/auth";

class UserController {
  constructor() {}

  async login(req: Request, res: Response) {
    let result;
    let user;
    try {
      result = await userService.login(req.body.email, req.body.password);
      user = createJWTToken();
      res.json({
        user,
      });
    } catch (error) {
      console.log(error);
    }
  }
}

export default UserController;
