import mongoose from "../database";

const schema = new mongoose.Schema({
  dna: [
    {
      type: String,
    },
  ],
  mutacion: Boolean,
});

const dnaRegister = mongoose.model("dnaRegister", schema);

export default dnaRegister;
