import { Router } from "express";
import UserController from "../controllers/user/user.controller";

export class MutationRouter {
  user = new UserController();
  router: Router;
  constructor() {
    this.router = Router();
    this.init();
  }

  init = () => {
    this.router.post("/", this.user.login);
  };
}
const MutationRoutes = new MutationRouter();
MutationRoutes.init();
export default MutationRoutes.router;
